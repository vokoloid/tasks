#include "../main.h"
#include "../inc/task_1.h"
/*
Input vector size: 5 
Input numbers: 1 3 3 5 1 
Input number to delete: 3 
Result: 1 5 1
*/
using namespace std;
vector <int> rmFromVector( vector <int> & inVec, int value)
{
    vector <int> newVector(0);
    for (int i =0;i<inVec.size();i++)
    {
        if (inVec[i]!=value)
        {
            inVec.pop_back();   
        }
        newVector.push_back(inVec[i]);
    }
    return newVector;
}

void task_1()
{
    int vecSize=0;
    cout << "Input vector size: ";
    cin >> vecSize;
    vector <int> numVec(vecSize);
    cout << "Input numbers: ";
    for (int i=0; i< numVec.size(); i++)
    {   int num;
        cin >> num;
        numVec[i]=num;
    }
    cout << "Input number to delete: ";
    int rmNumber=0;
    cin >> rmNumber;
    numVec = rmFromVector(numVec, rmNumber);
    cout << "Result ";
    for (int i=0;i<numVec.size();i++)
    {
        cout <<" "<< numVec[i];
    }
    cout << endl;

}