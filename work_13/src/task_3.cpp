#include "../main.h"
using namespace std;

class cyclic_buffer
{
    public: 
    
    cyclic_buffer( vector<int> * inVector)
    {
        p_vec=inVector;
        pop_position=0;
        push_position=0;

    };
    void push(int val);
    void write();
    int read(int position);
    int pop(); //в задании это оказалось не нужным, из-за того, что нужно каждый раз выводить целиком весь буфер

    void set_pop_position(int position){pop_position=position;}
    int get_pop_position(){return pop_position;}
    int get_push_position(){return push_position;}
    int size(){int size=push_position-pop_position; return (size)>0?size:size*(-1);}
    private:
    vector<int> *p_vec;
    int pop_position;
    int push_position;
};

void cyclic_buffer::push(int val)
{
    p_vec->at(push_position) = val;
    push_position++;
    if (push_position>19)
    {
        push_position=0;
    }

}
int cyclic_buffer::pop()
{
    int val = p_vec->at(pop_position);
    pop_position++;
    if (pop_position>19)
    {
        pop_position=0;
    }
    if (pop_position==push_position)
    {
        pop_position=push_position;  
   
    }
    return  val;
}
int cyclic_buffer::read(int position)
{
    return p_vec->at(position);    
}


void task_3()
{
    vector<int> db(20);
    cyclic_buffer buffer(&db);
    int val;
    int size = 0;
    int start = 0;
    while (1)
    {   
        cin >> val;
        if (val<0) {
            cout << "output: ";
            for (int i =0;i<size;i++)
            {   if ((i+start)<20)   cout <<" "<< buffer.read(i+start);
                else cout <<" "<< buffer.read(20-i);
            }
            cout <<endl;
            continue;
        };
        
        buffer.push(val);
        if (size<20) size++;
        else start++;
        if (start>20)start=0;
    }

}