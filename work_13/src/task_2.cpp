#include "../main.h"
using namespace std;

void task_2()
{
    std::vector<float> prices {2.5, 4.25, 3.0, 10.0};
    std::vector<int> items {1, 1, 0, 3, 5};
    int sum=0;
    int num = prices.size()>items.size()? prices.size():items.size();
    for (int i =0;i<num;i++)
    {   
        if (items[i]< prices.size())
            sum+=prices[items[i]];
    }
    
    cout << "\nСуммарная стоимость будет равна "<< sum<< endl;
}