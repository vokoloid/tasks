
# Записки к курсу

## Работа с векторами
``` cpp
//first realization
// #define SECOND_TRY
#ifdef FIRST_TRY
vector <int> addToVector( vector <int> & inVec, int num)
{
    //create new extended vector
    vector<int> vec(inVec.size()+1);
    //fill extended vector with old values
    for (int i =0; i<vec.size();i++)
       vec[i] = inVec[i];
    //add value for last element in new vector
    vec[vec.size()-1] = num;
    return vec;

}
vector <int> rmFromVector( vector <int> & inVec)
{
    //create new extended vector
    vector<int> vec(inVec.size()-1);
    //fill extended vector with old values
    for (int i =0; i<vec.size();i++)
       vec[i] = inVec[i];
    //add value for last element in new vector
    return vec;
}
#elseif SECOND_TRY
void addToVector( vector <int> * inVec, int num)
{
    //create new extended vector
    inVec->resize(inVec->size()+1);
    inVec[inVec->size()-1] = num;
    return ;

}
void rmFromVector( vector <int> * inVec)
{
    //create new extended vector
    inVec->resize(inVec->size()-1);
    return ;
}
#endif
```